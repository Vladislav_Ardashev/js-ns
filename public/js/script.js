$(document).ready(function () {

    // координаты мыши над canvas.
    // необходимы для рисования
    var lastX, lastY;

    // инициализация canvas
    initCanvas();

    // обработчик загрузки изображения
    $('#upload').on('change', function(e) {
        handleFileSelect(e);
    });

    $('.modal_wrapper').click(function() {
        hideModal();
    });

    // обработчик сообщения от сервера
    window.socket = io();
    socket.on('predicted', function(msg){
        showModal(msg);
    });

});

/**
 * Инициализация canvas
 */
function initCanvas() {
    var $canvas = $('#canvas'),
        ctx = $canvas.get(0).getContext("2d"),
        mousePressed = false;

    clearCanvas($('#canvas').get(0));

    // рисование на canvas
    $canvas.mousedown(function (e) {
        mousePressed = true;
        draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false, ctx);
    }).mousemove(function (e) {
        if (mousePressed) {
            draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true, ctx);
        }
    }).mouseup(function () {
        mousePressed = false;
    }).mouseleave(function () {
        mousePressed = false;
    });
}

/**
 * Рисование на canvas
 * @param x
 * @param y
 * @param isDown
 * @param ctx
 */
function draw(x, y, isDown, ctx) {
    if (isDown) {
        ctx.beginPath();
        ctx.strokeStyle = "black";
        ctx.lineWidth = 10;
        ctx.lineJoin = "round";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
    }
    lastX = x;
    lastY = y;
}

function showModal(digit) {
    $('#predicted').html(digit);
    $('.modal_wrapper').fadeIn(200);
}

function hideModal() {
    $('.modal_wrapper').fadeOut(200);
}

/**
 * Загрузка изображения в canvas
 * @param event
 */
function handleFileSelect(event) {
    var files = event.target.files, // файлы из инпута
        $canvas = $('#canvas'),
        ctx = $canvas.get(0).getContext("2d");

    for (var i = 0, file; file = files[i]; i++) {

        // выйдем, если файл не является изображением
        if (!file.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        reader.onload = function(e) {
            var img = new Image();
            img.src = e.target.result;
            img.onload = function(){
                ctx.drawImage(this, 0, 0, 150, 150);
                generateNoise($canvas.get(0))
            };
        };

        reader.readAsDataURL(file);
    }
}

/**
 * Очистить canvas
 */
function clearCanvas() {
    var ctx = $('#canvas').get(0).getContext("2d");
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

/**
 * Добавить шум
 */
function addNoise() {
    var canvas = $('#canvas').get(0);
    generateNoise(canvas);
}

/**
 * Обработка изображения и отправка на вход сети
 */
function predict(){
    // отправим файл на сервер
    socket.emit('predict', $('#canvas').get(0).toDataURL());
}

/**
 * Генератор шума
 * @param canvas
 */
function generateNoise(canvas) {

    var ctx = canvas.getContext('2d'),
        x, y,
        number,
        opacity = 1;

    for (x = 0; x < canvas.width; x++) {
        for (y = 0; y < canvas.height; y++) {
            var threshold = Math.random() * (.9 - .5) + .5;
            if (Math.random() > threshold) {
                number = Math.floor( Math.random()  * 255);
                ctx.fillStyle = "rgba(" + number + "," + number + "," + number + "," + opacity + ")";
                ctx.fillRect(x, y, 1, 1);
            }
        }
    }

}